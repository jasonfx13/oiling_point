<?php
/**
 * The template for displaying search forms.
 *
 * @link https://developer.wordpress.org/reference/functions/get_search_form/
 *
 * @package op2019
 * @since op2019 0.9
 * @license GPL 2.0
 */
?>

<form method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
	<label>
		<span class="screen-reader-text"><?php esc_html_e( 'Search for:', 'op2019' ); ?></span>
		<input type="search" class="search-field" placeholder="<?php esc_attr_e( 'Search', 'op2019' ); ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label', 'op2019' ) ?>" />
	</label>
	<input type="submit" class="search-submit" value="<?php echo esc_attr_x( 'Search', 'submit button', 'op2019' ) ?>" />
</form>
