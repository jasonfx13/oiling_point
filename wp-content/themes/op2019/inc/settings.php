<?php

/**
 * Initialize the settings.
 */
function op2019_theme_settings(){
	$settings = SiteOrigin_Settings::single();

	$settings->add_section( 'header', esc_html__( 'Header', 'op2019' ) );
	$settings->add_section( 'navigation', esc_html__( 'Navigation', 'op2019' ) );
	$settings->add_section( 'layout', esc_html__( 'Layout', 'op2019' ) );
	$settings->add_section( 'home', esc_html__( 'Home', 'op2019' ) );
	$settings->add_section( 'pages', esc_html__( 'Pages', 'op2019' ) );
	$settings->add_section( 'blog', esc_html__( 'Blog', 'op2019' ) );
	$settings->add_section( 'comments', esc_html__( 'Comments', 'op2019' ) );
	$settings->add_section( 'social', esc_html__( 'Social', 'op2019' ) );
	$settings->add_section( 'footer', esc_html__( 'Footer', 'op2019' ) );
	$settings->add_section( 'text', esc_html__( 'Site Text', 'op2019' ) );

	// Header.
	$settings->add_field( 'header', 'logo', 'media', esc_html__( 'Logo', 'op2019' ), array(
		'description' => esc_html__( 'Your own custom logo.', 'op2019' )
	) );

	$settings->add_teaser( 'header', 'image_retina', 'media', esc_html__( 'Retina Logo', 'op2019' ), array(
		'choose' => esc_html__( 'Choose Image', 'op2019' ),
		'update' => esc_html__( 'Set Logo', 'op2019' ),
		'description' => esc_html__( 'A double sized version of your logo for use on high pixel density displays. Must be used in addition to standard logo.', 'op2019' ),
	) );

	$settings->add_field( 'header', 'tagline', 'checkbox', esc_html__( 'Tagline', 'op2019' ), array(
		'description' => esc_html__( 'Display the website tagline.', 'op2019' )
	) );	

	$settings->add_field( 'header', 'top_bar', 'checkbox', esc_html__( 'Top Bar', 'op2019' ), array(
		'description' => esc_html__( 'Display the top bar.', 'op2019' )
	) );

	$settings->add_field( 'header', 'display', 'checkbox', esc_html__( 'Header', 'op2019' ), array(
		'description' => esc_html__( 'Display the header.', 'op2019' )
	) );		

	$settings->add_field( 'header', 'layout', 'select', esc_html__( 'Header Layout', 'op2019' ), array(
		'options' => array(
			'default' => esc_html__( 'Default', 'op2019' ),
			'centered' => esc_html__( 'Centered', 'op2019'),
		),
		'description' => esc_html__( 'Select the header layout.', 'op2019')
	) );

	$settings->add_field( 'header', 'sticky', 'checkbox', esc_html__( 'Sticky Header', 'op2019' ), array(
		'description' => esc_html__( 'Sticks the header to the top of the screen as the user scrolls down.', 'op2019' )
	) );

	$settings->add_field( 'header', 'sticky_mobile', 'checkbox', esc_html__( 'Mobile Sticky Header', 'op2019' ), array(
		'description' => esc_html__( 'Use the sticky header on mobile devices.', 'op2019' )
	) );		

	$settings->add_field( 'header', 'opacity', 'text', esc_html__( 'Sticky Header Opacity', 'op2019' ), array(
		'description' => esc_html__( 'Set the header background opacity once it turns sticky. 0.1 (lowest) - 1 (highest).', 'op2019' )
	) );		

	$settings->add_field( 'header', 'scale', 'checkbox', esc_html__( 'Sticky Header Scaling', 'op2019' ), array(
		'description' => esc_html__( 'Scale the header down as it becomes sticky.', 'op2019' )
	) );		

	// Navigation.
	$settings->add_field('navigation', 'top_bar_menu', 'checkbox', __('Top Bar Menu', 'op2019'), array(
		'description' => __('Display the right top bar menu.', 'op2019')
	) );

	$settings->add_field('navigation', 'responsive_top_bar', 'checkbox', __('Responsive Top Bar', 'op2019'), array(
		'description' => __('Collapse the top bar for small screen devices.', 'op2019')
	) );	

	$settings->add_field('navigation', 'primary_menu', 'checkbox', __('Primary Menu', 'op2019'), array(
		'description' => __('Display the primary menu.', 'op2019')
	) );	

	$settings->add_field('navigation', 'responsive_menu', 'checkbox', __('Responsive Menu', 'op2019'), array(
		'description' => __('Use a special responsive menu for small screen devices. Requires the Primary Menu setting to be enabled.', 'op2019')
	) );		

	$settings->add_field('navigation', 'responsive_menu_collapse', 'number', __('Responsive Menu Collapse', 'op2019'), array(
		'description' => __('The pixel resolution when the primary menu and top bar collapse.', 'op2019')
	) );

	$settings->add_field('navigation', 'menu_search', 'checkbox', __('Menu Search', 'op2019'), array(
		'description' => __('Display a search icon in the main menu.', 'op2019')
	) );

	$settings->add_field('navigation', 'smooth_scroll', 'checkbox', __('Smooth Scroll', 'op2019'), array(
		'description' => __('Smooth scroll for internal anchor links from the main menu.', 'op2019')
	) );	

	$settings->add_field('navigation', 'breadcrumb_trail', 'checkbox', __('Breadcrumb Trail', 'op2019'), array(
		'description' => __('Display a breadcrumb trail below the menu. De-activate this setting if using Yoast Breadcrumbs or Breadcrumb NavXT.', 'op2019')
	) );		

	$settings->add_field('navigation', 'post_nav', 'checkbox', __('Post Navigation', 'op2019'), array(
		'description' => __('Display next/previous post navigation.', 'op2019')
	) );		

	$settings->add_field('navigation', 'scroll_top', 'checkbox', __('Scroll to Top', 'op2019'), array(
		'description' => __('Display the scroll to top button.', 'op2019')
	) );

	$settings->add_field('navigation', 'scroll_top_mobile', 'checkbox', __('Mobile Scroll to Top', 'op2019'), array(
		'description' => __('Display the scroll to top button on mobile devices.', 'op2019')
	) );					

	// Layout.
	$settings->add_field('layout', 'bound', 'select', __('Layout Bound', 'op2019'), array(
		'options' => array(
			'full' => __('Full Width', 'op2019'),
			'boxed' => __('Boxed', 'op2019'),
		),
		'description' => __('Select a full width or boxed theme layout.', 'op2019')
	) );

	$settings->add_field( 'layout', 'responsive', 'checkbox', __('Responsive Layout', 'op2019'), array(
		'description' => __('Adapt the site layout for mobile devices.', 'op2019')
	) );	

	$settings->add_field( 'layout', 'fitvids', 'checkbox', __('FitVids.js', 'op2019'), array(
		'description' => __('Include FitVids.js for fluid width video embeds.', 'op2019')
	));			

	// Home.
	$description = '';
	if ( ! class_exists( 'MetaSliderPlugin' ) && ! class_exists( 'SmartSlider3' ) ) {
		$description = sprintf(
			__( 'This theme supports <a href="%s" target="_blank">Smart Slider 3</a>. <a href="%s">Install it</a> for free to create beautiful responsive sliders - <a href="%s" target="_blank">More Info</a>', 'op2019' ),
			'https://purothemes.com/smart-slider-3/',
			op2019_smartslider_install_link(),
			'https://purothemes.com/documentation/op2019-theme/home-page-slider/'
		);
	}
	
	$settings->add_field( 'home', 'slider', 'select', __('Home Page Slider', 'op2019'), array(
		'options' => op2019_sliders_get_options( true ),
		'description' => $description,
	));
	
	$settings->add_field( 'home', 'slider_stretch', 'checkbox', __( 'Stretch Home Slider', 'op2019' ), array(
		'label'       => __( 'Stretch', 'op2019' ),
		'description' => __( 'Stretch the home page slider to the width of the screen if using the full width layout.', 'op2019' ),
	) );

	$settings->add_field( 'home', 'header_overlaps', 'checkbox', __('Header Overlaps Slider', 'op2019'), array(
		'description' => __('Should the header overlap the home page slider?', 'op2019')
	) );

	// Pages.
	$settings->add_field( 'pages', 'featured_image', 'checkbox', __('Featured Image', 'op2019'), array(
		'description' => __('Display the featured image on pages.', 'op2019')
	) );

	// Blog.
	$settings->add_field('blog', 'page_title', 'text', __('Blog Page Title', 'op2019'), array(
		'description' => __('The page title of the blog page.', 'op2019')
	) );

	$settings->add_field('blog', 'archive_layout', 'select', __('Blog Archive Layout', 'op2019'), array(
		'options' => op2019_blog_layout_options(),
		'description' => __('Choose the layout to be used on blog and archive pages.', 'op2019')
	) );

	$settings->add_field('blog', 'archive_featured_image', 'checkbox', __('Archive Featured Image', 'op2019'), array(
		'description' => __('Display the featured image on the blog archive pages.', 'op2019')
	) );	

	$settings->add_field('blog', 'archive_content', 'select', __('Archive Post Content', 'op2019'), array(
		'options' => array(
			'full' => __('Full Post Content', 'op2019'),
			'excerpt' => __('Post Excerpt', 'op2019'),
		),
		'description' => __('Choose how to display your post content on blog and archive pages. Select Full Post Content if using the "more" quicktag.', 'op2019'),
	));			

	$settings->add_field('blog', 'read_more', 'text', __('Read More Text', 'op2019'), array(
		'description' => __('The link text displayed when posts are split using the "more" quicktag.', 'op2019'),
	));	

	$settings->add_field('blog', 'excerpt_length', 'number', __('Post Excerpt Length', 'op2019'), array(
		'description' => __('If no manual post excerpt is added one will be generated. How many words should it be?', 'op2019'),
		'sanitize_callback' => 'absint'	
	));

    $settings->add_field('blog', 'excerpt_more', 'checkbox', __('Post Excerpt Read More Link', 'op2019'), array(
        'description' => __('Display the Read More text below the post excerpt. Only applicable if Post Excerpt has been selected from the Archive Post Content setting.', 'op2019'),
    ));    	

	$settings->add_field('blog', 'post_featured_image', 'checkbox', __('Post Featured Image', 'op2019'), array(
		'description' => __('Display the featured image on the single post page.', 'op2019')
	) );	

	$settings->add_field('blog', 'post_date', 'checkbox', __('Post Date', 'op2019'), array(
		'description' => __('Display the post date.', 'op2019')
	));			

	$settings->add_field('blog', 'post_author', 'checkbox', __('Post Author', 'op2019'), array(
		'description' => __('Display the post author.', 'op2019')
	));	

	$settings->add_field('blog', 'post_comment_count', 'checkbox', __('Post Comment Count', 'op2019'), array(
		'description' => __('Display the post comment count.', 'op2019')
	));		

	$settings->add_field('blog', 'post_cats', 'checkbox', __('Post Categories', 'op2019'), array(
		'description' => __('Display the post categories.', 'op2019')
	));		

	$settings->add_field('blog', 'post_tags', 'checkbox', __('Post Tags', 'op2019'), array(
		'description' => __('Display the post tags.', 'op2019')
	));

	$settings->add_field('blog', 'post_author_box', 'checkbox', __('Post Author Box', 'op2019'), array(
		'description' => __('Display the post author biographical info.', 'op2019')
	));			

	$settings->add_field('blog', 'related_posts', 'checkbox', __('Related Posts', 'op2019'), array(
		'description' => __('Display related posts on the single post page.', 'op2019')
	));		

	$settings->add_field( 'blog', 'edit_link', 'checkbox', __( 'Edit Link', 'op2019' ), array(
		'description' => __( 'Display an Edit link below post content. Visible if a user is logged in and allowed to edit the content. Also applies to Pages.', 'op2019' )
	) );

	// Comments.
	$settings->add_field('comments', 'allowed_tags', 'checkbox', __('Comment Form Allowed Tags', 'op2019'), array(
		'description' => __('Display the explanatory text below the comment form that lets users know which HTML tags may be used.', 'op2019')
	) );	

	$settings->add_teaser('comments', 'ajax_comments', 'checkbox', __('AJAX Comments', 'op2019'), array(
		'description' => __('Allow users to submit comments without a page re-load.', 'op2019'),
	));	 	

	// Social.
	$settings->add_teaser('social', 'share_post', 'checkbox', __('Post Sharing', 'op2019'), array(
		'description' => __('Show icons to share your posts on Facebook, Twitter, Google+ and LinkedIn.', 'op2019'),
	));	

	// Footer.
	$settings->add_field( 'footer', 'copyright_text', 'text', __( 'Copyright Text', 'op2019' ), array(
		'description' => __( '{site-title}, {copyright} and {year} can be used to display your website title, a copyright symbol and the current year.', 'op2019' ),
		'sanitize_callback' => 'wp_kses_post'
	) );

	$settings->add_field('footer', 'js_enqueue', 'checkbox', __('Enqueue JavaScript in Footer', 'op2019'), array(
		'description' => __('Enqueue theme JavaScript files in the footer. Doing so can improve site load time.', 'op2019'),
	));		

	$settings->add_teaser('footer', 'attribution', 'checkbox', __('Footer Attribution Link', 'op2019'), array(
		'description' => __('Remove the theme attribution link from your footer without editing any code.', 'op2019'),
	));			

	// Site Text.
	$settings->add_field( 'text', 'phone', 'text', __( 'Phone Number', 'op2019' ), array(
		'description' => __( 'A phone number displayed in the top bar. Use international dialing format to enable click to call.', 'op2019' )
	) );	

	$settings->add_field( 'text', 'email', 'text', __( 'Email Address', 'op2019' ), array(
		'description' => __( 'An email address to be displayed in the top bar', 'op2019' )
	) );				

	$settings->add_field( 'text', 'comments_closed', 'text', __( 'Comments Closed', 'op2019' ), array(
		'description' => __( 'The text visitors see at the bottom of posts when comments are closed.', 'op2019' )
	) );

	$settings->add_field( 'text', 'no_results_heading', 'text', __( 'No Search Results Heading', 'op2019' ), array(
		'description' => __( 'The search page heading visitors see when no results are found.', 'op2019' )
	) );		

	$settings->add_field( 'text', 'no_results_copy', 'text', __( 'No Search Results Text', 'op2019' ), array(
		'description' => __( 'The search page text visitors see when no results are found.', 'op2019' )
	) );	

	$settings->add_field( 'text', '404_heading', 'text', __( '404 Error Page Heading', 'op2019' ), array(
		'description' => __( 'The heading visitors see when no page is found.', 'op2019' )
	) );		

	$settings->add_field( 'text', '404_copy', 'text', __( '404 Error Page Text', 'op2019' ), array(
		'description' => __( 'The text visitors see no page is found.', 'op2019' )
	) );			
}
add_action('siteorigin_settings_init', 'op2019_theme_settings');

/**
 * Add default settings.
 *
 * @param $defaults
 *
 * @return mixed
 */
function op2019_settings_defaults( $defaults ){
	$defaults['header_logo'] = false;
	$defaults['header_image_retina'] = false;
	$defaults['header_tagline'] = false;
	$defaults['header_top_bar'] = true;
	$defaults['header_display'] = true;
	$defaults['header_layout'] = 'default';
	$defaults['header_sticky'] = true;
	$defaults['header_sticky_mobile'] = false;
	$defaults['header_opacity'] = 1;
	$defaults['header_scale'] = true;

	$defaults['navigation_top_bar_menu'] = true;
	$defaults['navigation_responsive_top_bar'] = false;
	$defaults['navigation_primary_menu'] = true;
	$defaults['navigation_responsive_menu'] = true;
	$defaults['navigation_responsive_menu_collapse'] = 1024;
	$defaults['navigation_menu_search'] = true;
	$defaults['navigation_smooth_scroll'] = true;
	$defaults['navigation_breadcrumb_trail'] = false;
	$defaults['navigation_post_nav'] = true;
	$defaults['navigation_scroll_top'] = true;
	$defaults['navigation_scroll_top_mobile'] = false;

	$defaults['layout_bound'] = 'full';
	$defaults['layout_responsive'] = true;
	$defaults['layout_fitvids'] = true;		

	$defaults['home_slider'] = 'demo';
	$defaults['home_slider_stretch'] = true;
	$defaults['home_header_overlaps'] = false;

	$defaults['pages_featured_image'] = true;

	$defaults['blog_page_title'] = esc_html__( 'Blog', 'op2019' );
	$defaults['blog_archive_layout'] = 'blog';
	$defaults['blog_archive_featured_image'] = true;
	$defaults['blog_archive_content'] = 'full';
	$defaults['blog_read_more'] = esc_html__( 'Continue reading', 'op2019' );
	$defaults['blog_excerpt_length'] = 55;
	$defaults['blog_excerpt_more'] = false;
	$defaults['blog_post_featured_image'] = true;
	$defaults['blog_post_date'] = true;
	$defaults['blog_post_author'] = true;
	$defaults['blog_post_comment_count'] = true;
	$defaults['blog_post_cats'] = true;
	$defaults['blog_post_tags'] = true;
	$defaults['blog_post_author_box'] = false;
	$defaults['blog_related_posts'] = false;				
	$defaults['blog_edit_link'] = true;	

	$defaults['comments_allowed_tags'] = true;
	$defaults['comments_ajax_comments'] = true;

	$defaults['social_share_post'] = true;
	$defaults['social_share_page'] = false;
	$defaults['social_twitter'] = '';			

	$defaults['footer_copyright_text'] = esc_html__( '{copyright} {year} {site-title}', 'op2019' );
	$defaults['footer_attribution'] = true;
	$defaults['footer_js_enqueue'] = false;

	$defaults['text_phone'] = esc_html__( '1800-345-6789', 'op2019' );
	$defaults['text_email'] = esc_html__( 'info@yourdomain.com', 'op2019' );
	$defaults['text_comments_closed'] = esc_html__( 'Comments are closed.', 'op2019' );
	$defaults['text_no_results_heading'] = esc_html__( 'Nothing Found', 'op2019' );
	$defaults['text_no_results_copy'] = esc_html__( 'Sorry, but nothing matched your search terms. Please try again with some different keywords. ', 'op2019');
	$defaults['text_404_heading'] = esc_html__( 'Oops! That page can\'t be found.', 'op2019' );
	$defaults['text_404_copy'] = esc_html__( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'op2019' );

	return $defaults;
}
add_filter('siteorigin_settings_defaults', 'op2019_settings_defaults');

function op2019_blog_layout_options() {
	$layouts = array();
	foreach ( glob( get_template_directory().'/loops/loop-*.php') as $template ) {
		$headers = get_file_data( $template, array(
			'loop_name' => 'Loop Name',
		) );

		preg_match( '/loop\-(.*?)\.php/', basename( $template ), $matches );
		if ( ! empty( $matches[1] ) ) {
			$layouts[$matches[1]] = $headers['loop_name'];
		}
	}
	return $layouts;
}

function op2019_siteorigin_settings_home_slider_update_post_meta( $new_value, $old_value ) {

	// Update home slider post meta.
	$home_id = get_option( 'page_on_front' );
	if ( $home_id ) {
		update_post_meta( $home_id, 'op2019_metaslider_slider', siteorigin_setting( 'home_slider' ) );
 		update_post_meta( $home_id, 'op2019_metaslider_slider_stretch', siteorigin_setting( 'home_slider_stretch' ) );
 		update_post_meta( $home_id, 'op2019_metaslider_slider_overlap', siteorigin_setting( 'home_header_overlaps' ) );
	}
	return $new_value;
}
add_filter( 'update_option_theme_mods_op2019', 'op2019_siteorigin_settings_home_slider_update_post_meta', 10, 2 );

/**
 * Localize the theme settings.
 */
function op2019_siteorigin_settings_localize( $loc ){
	$loc = array(
		'section_title' 			=> esc_html__( 'Theme Settings', 'op2019' ),
		'section_description' 		=> esc_html__( 'Settings for your theme.', 'op2019' ),
		'premium_only'				=> esc_html__( 'Premium Only', 'op2019' ),
		'premium_url' 				=> '#',
		// For the settings metabox.
		'meta_box'            		=> esc_html__( 'Page Settings', 'op2019' ),
		// For archives section
		'page_section_title' 		=> esc_html__( 'Page Template Settings', 'op2019' ),
		'page_section_description' 	=> esc_html__( 'Change layouts for various pages on your site.', 'op2019' ),
		// For all the different temples and template types
		'template_home' 			=> esc_html__( 'Blog Page', 'op2019' ),
		'template_search' 			=> esc_html__( 'Search Results', 'op2019' ),
		'template_date' 			=> esc_html__( 'Date Archives', 'op2019' ),
		'template_404' 				=> esc_html__( 'Not Found', 'op2019' ),
		'template_author' 			=> esc_html__( 'Author Archives', 'op2019' ),
		'templates_post_type' 		=> esc_html__( 'Type', 'op2019' ),
		'templates_taxonomy' 		=> esc_html__( 'Taxonomy', 'op2019' ),
	);
	return $loc;
}
add_filter( 'siteorigin_settings_localization', 'op2019_siteorigin_settings_localize' );

/**
 * Setup Page Settings for op2019.
 */
function op2019_page_settings( $settings, $type, $id ){

	$settings['layout'] = array(
		'type'    => 'select',
		'label'   => esc_html__( 'Page Layout', 'op2019' ),
		'options' => array(
			'default'            => esc_html__( 'Default', 'op2019' ),
			'no-sidebar'         => esc_html__( 'No Sidebar', 'op2019' ),
			'full-width'         => esc_html__( 'Full Width', 'op2019' ),
			'full-width-sidebar' => esc_html__( 'Full Width, With Sidebar', 'op2019' ),
		),
	);

	$settings['display_top_bar'] = array(
		'type'           => 'checkbox',
		'label'          => esc_html__( 'Top Bar', 'op2019' ),
		'checkbox_label' => esc_html__( 'Enable', 'op2019' ),
		'description'    => esc_html__( 'Display the top bar. Global setting must be enabled.', 'op2019' )
	);		

	$settings['display_header'] = array(
		'type'           => 'checkbox',
		'label'          => esc_html__( 'Header', 'op2019' ),
		'checkbox_label' => esc_html__( 'Enable', 'op2019' ),
		'description'    => esc_html__( 'Display the header. Global setting must be enabled.', 'op2019' )
	);	

	$settings['header_margin'] = array(
		'type'           => 'checkbox',
		'label'          => esc_html__( 'Header Bottom Margin', 'op2019' ),
		'checkbox_label' => esc_html__( 'Enable', 'op2019' ),
		'description'    => esc_html__( 'Display the margin below the header.', 'op2019' )
	);	

	$settings['page_title'] = array(
		'type'           => 'checkbox',
		'label'          => esc_html__( 'Page Title', 'op2019' ),
		'checkbox_label' => esc_html__( 'Enable', 'op2019' ),
		'description'    => esc_html__( 'Display the page title.', 'op2019' )
	);

	$settings['footer_margin'] = array(
		'type'           => 'checkbox',
		'label'          => esc_html__( 'Footer Top Margin', 'op2019' ),
		'checkbox_label' => esc_html__( 'Enable', 'op2019' ),
		'description'    => esc_html__( 'Display the margin above the footer.', 'op2019' )
	);

	$settings['display_footer_widgets'] = array(
		'type'           => 'checkbox',
		'label'          => esc_html__( 'Footer Widgets', 'op2019' ),
		'checkbox_label' => esc_html__( 'Enable', 'op2019' ),
		'description'    => esc_html__( 'Display the footer widgets.', 'op2019' )
	);

	return $settings;
}
add_filter( 'siteorigin_page_settings', 'op2019_page_settings', 10, 3 );

/**
 * Add the default Page Settings.
 */
function op2019_setup_page_setting_defaults( $defaults, $type, $id ) {
	$defaults['layout']              	= 'default';
	$defaults['display_top_bar']   		= true;
	$defaults['display_header']      	= true;
	$defaults['header_margin']     		= true;
	$defaults['page_title']          	= true;
	$defaults['display_footer_widgets'] = true;
	$defaults['footer_margin']       	= true;

	return $defaults;
}
add_filter( 'siteorigin_page_settings_defaults', 'op2019_setup_page_setting_defaults', 10, 3 );

function op2019_page_settings_message( $post ){
	if( $post->post_type == 'page' ) {
		?>
		<div class="so-page-settings-message" style="background-color: #f3f3f3; padding: 10px; margin-top: 12px; border: 1px solid #d0d0d0">
			<?php _e( 'To use these page settings, please use the <strong>Default</strong> template selected under <strong>Page Attributes</strong>.', 'op2019' ) ?>
		</div>
		<?php
	}
}
add_action( 'siteorigin_settings_before_page_settings_meta_box', 'op2019_page_settings_message' );
