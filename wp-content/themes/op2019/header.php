<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package op2019
 * @since op2019 0.9
 * @license GPL 2.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-N72K5LP');</script>
  <!-- End Google Tag Manager -->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N72K5LP"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'op2019' ); ?></a>

	<?php if ( siteorigin_setting( 'header_top_bar' ) && siteorigin_page_setting( 'display_top_bar', true ) ) : ?>
		<?php get_template_part( 'parts/top-bar' ); ?>
	<?php endif; ?>

	<?php if ( siteorigin_setting( 'header_display' ) && siteorigin_page_setting( 'display_header', true ) ) : ?>
		<header id="masthead" class="site-header<?php if ( siteorigin_setting( 'header_sticky' ) ) echo ' sticky-header'; if ( siteorigin_setting( 'header_scale' ) ) echo ' scale'; if ( siteorigin_setting( 'navigation_responsive_menu' ) ) echo ' responsive-menu'; ?>">
			<div class="container">
				<div class="site-branding-container">
					<div class="site-branding">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
							<?php op2019_display_logo(); ?>
						</a>
						<?php if ( get_bloginfo( 'description' ) && siteorigin_setting( 'header_tagline' ) ) : ?>
							<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
						<?php endif; ?>
					</div><!-- .site-branding -->
				</div><!-- .site-branding-container -->

				<nav id="site-navigation" class="main-navigation">
					<?php do_action( 'op2019_before_nav' ); ?>
					<?php if ( siteorigin_setting( 'navigation_primary_menu' ) ) wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
					<?php if ( siteorigin_setting( 'navigation_menu_search' ) ) : ?>
						<div class="menu-search">
							<div class="search-icon"></div>
							<form method="get" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
								<input type="text" class="field" name="s" value="<?php echo esc_attr( get_search_query() ); ?>" />
							</form>	
						</div><!-- .menu-search -->
					<?php endif; ?>
				</nav><!-- #site-navigation -->
			</div><!-- .container -->
		</header><!-- #masthead -->
	<?php endif; ?>

	<?php op2019_render_slider(); ?>

	<div id="content" class="site-content">

		<?php if ( class_exists( 'WooCommerce' ) && is_woocommerce() ) get_template_part( 'parts/woocommerce-title' ); ?>
