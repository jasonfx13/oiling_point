<?php
/**
 * Part Name: WooCommerce Title.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 * 
 * @package op2019
 * @since op2019 1.0.2
 * @license GPL 2.0
 */
?>

<header class="entry-header">
	<div class="container">
		<?php do_action( 'op2019_woocommerce_title' ); ?><?php do_action( 'op2019_woocommerce_breadcrumb' ); ?>
	</div><!-- .container -->
</header><!-- .entry-header -->	
