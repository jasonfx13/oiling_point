<?php

function op2019_premium_upgrade_content( $content ) {
	$content['premium_title'] = esc_html__( 'Upgrade to op2019 Premium', 'op2019' );
	$content['premium_summary'] = esc_html__( 'Hi, my name is Andrew Misplon, the developer of op2019. If you\'ve enjoyed op2019 Free then I know you\'re going to love op2019 Premium. Below you\'ll find an outline of the premium features.', 'op2019' );

	$content['buy_url'] = 'http://puro.fetchapp.com/sell/49b42a54';
	$content['premium_video_poster'] = get_template_directory_uri() . '/upgrade/poster.jpg';

	$content['features'] = array();

	$content['features'][] = array(
		'heading' => esc_html__( 'Premium Email Support', 'op2019' ),
		'content' => esc_html__( 'op2019 Premium comes with priority email support. Let us know if you run into any challenges or simply need a hand getting setup. We\'re here to help.', 'op2019' ),
	);

	$content['features'][] = array(
		'heading' => esc_html__( 'Name the Price', 'op2019' ),
		'content' => esc_html__( 'You choose the price, so you can pay what op2019 is worth to you. Choose from one our suggested options or specify your own custom price. Regardless of what you pay you\'ll receive the same premium upgrade and be supporting the continued development of the theme.', 'op2019' ),
	);

	$content['features'][] = array(
		'heading' => esc_html__( 'Retina Logo', 'op2019' ),
		'content' => esc_html__( 'op2019 Premium allows you to upload an additional, double-sized logo, to be displayed on Apple Retina and other high pixel density displays.', 'op2019' ),
	);

	$content['features'][] = array(
		'heading' => esc_html__( 'Enhanced Customizer Integration', 'op2019' ),
		'content' => esc_html__( 'Make op2019 your own with enhanced Customizer integration. Choose from Google\'s huge selection of fonts; change colors, spacing and more all using the live-updating WordPress Customizer.', 'op2019' ),
		'image' => get_template_directory_uri() . '/upgrade/teasers/customizer.png',
	);	

	$content['features'][] = array(
		'heading' => esc_html__( 'Ajax Comments', 'op2019' ),
		'content' => esc_html__( 'Remove page re-loads from your comment forms. This means that users can submit comments without losing their place in a gallery or interrupting a video.', 'op2019' ),
		'image' => get_template_directory_uri() . '/upgrade/teasers/ajax-comments.png',
	);

	$content['features'][] = array(
		'heading' => esc_html__( 'Post Sharing', 'op2019' ),
		'content' => esc_html__( 'Add sharing icons for Facebook, Twitter, Google Plus and LinkedIn to the bottom of your posts.', 'op2019' ),
		'image' => get_template_directory_uri() . '/upgrade/teasers/social-sharing.png',
	);

	$content['features'][] = array(
		'heading' => esc_html__( 'Remove Attribution Link', 'op2019' ),
		'content' => esc_html__( 'op2019 Premium gives you the option to remove the "Theme by Puro" text from your Footer without editing any code.', 'op2019' ),
		'image' => get_template_directory_uri() . '/upgrade/teasers/attribution.png',
	);		

	$content['features'][] = array(
		'heading' => esc_html__( 'Continued Updates', 'op2019' ),
		'content' => esc_html__( 'Your premium upgrade is a valuable contribution to the future development of op2019, ensuring it\'s compatible with future versions of WordPress.', 'op2019' ),
		'image' => get_template_directory_uri() . '/upgrade/teasers/updates.png',
	);			

	$content['testimonials'] = array(
		array(
			'gravatar' => '07079067e6b88e8d2a2a1fa886e50b98',
			'name' => 'Cloudontap',
			'title' => 'Beautiful Theme, Top-Notch Support!',
			'content' => esc_html__( 'LOVE this theme - so many options, and so gorgeous! I HIGHLY suggest submitting a question on their support forum if you run into any problems - they respond within minutes to my questions! Doesn\'t get much better than that. Thanks for all of your help, Andrew, and thank you for the awesome theme!', 'op2019' ),
		),
		array(
			'gravatar' => '054d90a54412053c65f897b01f367e4b',
			'name' => 'AJR Computing',
			'title' => 'Amazing Responsive Theme, with Awesome Support!',
			'content' => esc_html__( 'The op2019 WordPress theme looks awesome and the best part is it\'s a free theme, I really love this theme and my site is starting to come together and looks really sharp thanks to this amazing theme also the theme designer Andrew Misplon from Puro themes provided me with great support when I needed assistance in setting up a few things.', 'op2019' ),
		),					
		array(
			'gravatar' => '1bc72295a0b339d59610b7e1ac0bead2',
			'name' => 'Wccwts',
			'title' => 'Responsive',
			'content' => esc_html__( 'Theme is very responsive and so is the support. Highly recommended!.', 'op2019' ),
		),
		array(
			'gravatar' => 'ba93dfb175b0c3c8d437fbe1c9465a50',
			'name' => 'Sunshine4nikki',
			'title' => 'op2019 Impressed!',
			'content' => esc_html__( 'The op2019+ theme is great (my FAVORITE). I\'m impressed by the ease of set-up (even for a WP beginner). The versatility of design along with everything needed in the customizing menu gave me the ability to see a live preview before publishing. It\'s OBVIOUS that the author put a lot of effort into creating op2019+ because it\'s so easy to use - my admin board isn\'t cluttered with a bunch of plugins. The interface is clean and simple... perfect!', 'op2019' ),
		),				
	);	

	return $content;
}
add_filter( 'siteorigin_premium_content', 'op2019_premium_upgrade_content' );
